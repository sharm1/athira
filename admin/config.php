<?php
// HTTP
define('HTTP_SERVER', 'http://www.athiratrendz.com/admin/');
define('HTTP_CATALOG', 'http://www.athiratrendz.com/');

// HTTPS
define('HTTPS_SERVER', 'http://www.athiratrendz.com/admin/');
define('HTTPS_CATALOG', 'http://www.athiratrendz.com/');

// DIR
define('DIR_APPLICATION', '/usr/share/nginx/html/athira/admin/');
define('DIR_SYSTEM', '/usr/share/nginx/html/athira/system/');
define('DIR_LANGUAGE', '/usr/share/nginx/html/athira/admin/language/');
define('DIR_TEMPLATE', '/usr/share/nginx/html/athira/admin/view/template/');
define('DIR_CONFIG', '/usr/share/nginx/html/athira/system/config/');
define('DIR_IMAGE', '/usr/share/nginx/html/athira/image/');
define('DIR_CACHE', '/usr/share/nginx/html/athira/system/cache/');
define('DIR_DOWNLOAD', '/usr/share/nginx/html/athira/system/download/');
define('DIR_UPLOAD', '/usr/share/nginx/html/athira/system/upload/');
define('DIR_LOGS', '/usr/share/nginx/html/athira/system/logs/');
define('DIR_MODIFICATION', '/usr/share/nginx/html/athira/system/modification/');
define('DIR_CATALOG', '/usr/share/nginx/html/athira/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'athira');
define('DB_PASSWORD', 'shyam788');
define('DB_DATABASE', 'athira');
define('DB_PREFIX', 'oc_');
